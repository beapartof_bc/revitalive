import $ from 'jquery';

// add the line below to disable check
/* eslint-disable */

	setTimeout(initChartSizer,100);

	//init ChartSizer
    function initChartSizer(){
        var pagelisting = document.querySelector(".productView");
        
        //if on a product page
        if(document.body.contains(pagelisting)){     
        	//looks for it sizes option is on product  
        	if($(".productView:contains('SIZE:'), .productView:contains('Size:')").length > 0){   

            	var sizeChart = document.createElement("a");
				sizeChart.style.cssText += "display:block;clear:both;width:100%;position:relative;min-height: 50px;margin-top: 0;";
				sizeChart.setAttribute("href","#");
				sizeChart.setAttribute("onclick","showProductImage('http://www.braveleather.com/fit-sizing-popup/', 1); return false;");
				sizeChart.className += "viewsizelink";
				sizeChart.innerHTML = "<img src='https://cdn2.bigcommerce.com/server1100/lno4u6fm/product_images/uploaded_images/find-your-size.jpg' />";
				
            
            	var sizeChartScript = document.createElement("script");
            	sizeChartScript.innerHTML = "function showProductImage(filename, product_id) {var l = (screen.availWidth/2)-350;var t = (screen.availHeight/2)-300;var imgPopup = window.open(filename + '?product_id='+product_id, 'imagePop', 'toolbar=0,scrollbars=1,location=0,statusbar=1,menubar=0,resizable=1,width=750,height=600,top='+t+',left='+l);imgPopup.focus();}";
            	document.querySelector('body').appendChild(sizeChartScript);
            	
            	
            	document.querySelector('div[data-product-option-change]').appendChild(sizeChart);
        	}  
        }   
    }
        

// add the line below to re-enable check
/* eslint-enable */

