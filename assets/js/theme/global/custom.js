import $ from 'jquery';

// add the line below to disable check
/* eslint-disable */

$(".navUser-action--quickSearch").click(function(){
$(".dropdown--quickSearch").toggleClass("search-closed search-opened"); 
    if ($("i",this).text() == "search") {
         $("i",this).text("close");
     }
      else {
         $("i",this).text("search");
     } 
});
 
// add the line below to re-enable check
/* eslint-enable */
