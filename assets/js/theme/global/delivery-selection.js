var _check_add_to_cart = null;

/**
 * contain functions used for the delivery option selection
 * @namespace DeliverySelection
 */
var DeliverySelection = {
	config: {},
	restrictedDates: [],
	minDate: false,
	maxDate: false,
	zipCode: '',
	stepsComplete: false,

	/**
	 * handles update to the selected delivery option
	 *
	 * @function handleDeliveryOptionChange
	 * @param {Object} event - event object, the options field is required as event data
	 */
	handleDeliveryOptionChange: function(event) {
		// missing option container references
		if (!event.data || !event.data.options) return;

		var selected = $(this).children('option:selected').text();
		var self = DeliverySelection;
		var event_opts = event.data.options;

		if (selected.toLowerCase() == 'pickup') {
			self.showOption(event_opts.pickup_location);
			self.hideOption(event_opts.delivery_zip);
			event_opts.pickup_location.select.trigger('change');
		} else if (selected.toLowerCase() == 'delivery') {
			self.showOption(event_opts.delivery_zip);
			self.hideOption(event_opts.pickup_location);
			self.hideOption(event_opts.wholesale_location);
		} else {
			self.hideOption(event_opts.delivery_zip);
			self.hideOption(event_opts.pickup_location);
			self.hideOption(event_opts.wholesale_location);
		}
	},

	/**
	 * handles update to the selected pickup location
	 *
	 * @function handlePickupLocationChange
	 * @param {Object} event - event object, the options field is required as event data
	 */
	handlePickupLocationChange: function(event) {
		// missing option container references
		if (!event.data || !event.data.options) return;

		var selected = $(this).children('option:selected').text();
		var self = DeliverySelection;
		var event_opts = event.data.options;

		if (selected.toLowerCase().indexOf('affiliate') > -1) {
			// wholesale pickup
			var config = self.config.delivery_opts.pickup.wholesale;
			self.showOption(event_opts.wholesale_location);
		} else {
			// store pickup
			var config = self.config.delivery_opts.pickup.store;
			self.hideOption(event_opts.wholesale_location);
		}

		// update restricted dates
		self.restrictedDates = self.parseRestrictedDates(config);
		if (event.data.datepicker) event.data.datepicker.datepicker('refresh');
	},

	/**
	 * returns the DOM elements associated with a product option label
	 *
	 * @function getOptionFromLabel
	 * @param {String} labelText - label text to search for
	 * @return {Object} - object containing reference to label, select and container elements
	 */
	getOptionFromLabel: function(labelText) {
		var labels = $('.productAttributeLabel .name');
		var selected = { 'label': false, 'container': false, 'select': false };

		labels.each(function(i, label) {
			if ($(label).text().trim().toLowerCase().indexOf(labelText.toLowerCase()) > -1) {
				selected.label = $(label);
				return false;
			}
		});

		// failed to find label
		if (!selected.label) return false;

		selected.container = selected.label.parents('.productAttributeRow');

		// failed to find container
		if (!selected.container) return false;
		
		selected.select = selected.container.find('.productAttributeValue .validation');
		if (!selected.select) return false;

		return selected;
	},

	/**
	 * show option row container
	 *
	 * @function showOption
	 * @param {Object} option - option row from options oject
	 * @param {boolean} animate - set to true to show animation
	 */
	showOption: function(option, animate) {
		var label = $(option.label).parents('.productAttributeLabel');
		var value = $(option.select).parents('.productAttributeValue');

		if (!animate) {
			label.show();
			value.show();
		} else {
			label.fadeIn(200);
			value.fadeIn(200);
		}
	},

	/**
	 * hide option row container
	 *
	 * @function hideOption
	 * @param {Object} option - option row container
	 * @param {boolean} animate - set to true to show animation
	 */
	hideOption: function(option, animate) {
		var label = $(option.label).parents('.productAttributeLabel');
		var value = $(option.select).parents('.productAttributeValue');

		if (!animate) {
			label.hide();
			value.hide();
		} else {
			label.fadeOut(200);
			value.fadeOut(200);
		}
	},

	/**
	 * select an option value in a dropdown
	 *
	 * @function selectOptionValue
	 * @param {Object} select - select node
	 * @param {String} value - option value to select
	 */
	selectOptionValue: function(select, value) {
		if ($(select).is('select')) {
			$(select).children('option:checked').removeAttr('selected');
			$(select).children('option[value="' + value + '"]').first().attr('selected', 'selected');
		} else {
			$(select).val(value);
		}
	},

	/**
	 * validate the date selection
	 *
	 * @function validateDateSelection
	 * @param {Object} selects
	 * @return {boolean}
	 */
	validateDateSelection: function(selects) {
		// no configuration, no restrictions
		if (!document.__bapo_config__) return true;

		// missing date selector
		if (!selects.day || !selects.month || !selects.year) return false;

		var day = selects.day.val();
		var month = selects.month.val();
		var year = selects.year.val();
		
		// not all date fields are selected
		if (day == "-1" || month == "-1" || year == "-1") return false;

		return true;
	},

	/**
	 * replace check_add_to_cart function with the given function, the original function will be accessible as _check_add_to_cart
	 *
	 * @function replaceAddToCartFunction
	 * @param {function} func
	 */
	replaceAddToCartFunction: function(func) {
		if (typeof func !== 'function') return;

		_check_add_to_cart = check_add_to_cart;
		check_add_to_cart = func;
	},

	/**
	 * replace ExpressCheckout.HandleResponse function with the given function, the original function will be accessible as ExpressCheckout._HandleResponse
	 *
	 * @function replaceCheckoutResponseHandler
	 * @param {function} func
	 */
	replaceCheckoutResponseHandler: function(func) {
		if (typeof func !== 'function') return;

		if (ExpressCheckout) {
			ExpressCheckout._HandleResponse = ExpressCheckout.HandleResponse;
			ExpressCheckout.HandleResponse = func;
		}
	},

	/**
	 * check if the given time greater than the cutoff time
	 *
	 * @function beforeCutoffTime
	 * @param {Object} time - moment object
	 * @return {boolean}
	 */
	beforeCutoffTime: function(time) {
		// convert time to cutoff timezone
		var compareTime = time.clone().tz(this.config.cutoff_time.timezone);
		var cutoffTime = moment().tz(this.config.cutoff_time.timezone).set('hour', this.config.cutoff_time.hour).set('minute', this.config.cutoff_time.minute).set('second', 0);

		return (compareTime.isBefore(cutoffTime));
	},

	/**
	 * filter the dates to display in jQuery.datepicker
	 * 
	 * @function filterRestrictedDate
	 * @param {String} date - formatted date passed from jQuery.datepicker
	 */
	filterRestrictedDate: function(date) {
		date = moment(date);
        return [ DeliverySelection.restrictedDates.indexOf(date.format('YYYY-MM-DD')) == -1 ];
	},

	/**
	 * parse config for a specific delivery option and returns the restricted dates
	 * 
	 * @function parseRestrictedDates
	 * @param {Object} config - configuration object for a delivery option
	 * @return {Array} - array of restricted dates formatted in YYYY-MM-DD
	 */
	parseRestrictedDates: function(opt_config, startDate, endDate) {
		var self = DeliverySelection;

		if (!opt_config.restriction_type || opt_config.restriction_type == 'none') {
			if (self.config.holidays) return self.config.holidays;

			return [];
		}

		// gather a list of dates
		var currentDate = (startDate) ? startDate.clone() : self.minDate.clone();
		var maxDate = (endDate) ? endDate : self.maxDate;

		var dates = [];

		while (!currentDate.isAfter(maxDate)) {
			dates.push(currentDate.clone());
			currentDate.add(1, 'days');
		}

		if (opt_config.restriction_type == 'dayOfWeek') {
			var values = opt_config.restricted_values;

			var restricted = dates.filter(function(date) {
				return (values.indexOf(date.format('ddd')) > -1);
			});
		} else if (opt_config.restriction_type == 'dayOfMonth') {
			var values = opt_config.restricted_values.map(function(val) {
				return parseInt(val);
			});

			var restricted = dates.filter(function(date) {
				return (values.indexOf(date.get('date')) > -1);
			});
		} else if (opt_config.restriction_type == 'date') {
			var values = opt_config.restricted_values.map(function(date) {
				return moment(date);
			});

			var restricted = dates.filter(function(date) {
				var isRestricted = values.filter(function(rDate) {
					return rDate.isSame(date);
				});

				return (isRestricted.length > 0);
			});
		} else {
			var restricted = dates;
		}

		// exclude holidays
		if (self.config.holidays) {
			restricted = restricted.map(function(date) {
				return date.format('YYYY-MM-DD');
			});

			return restricted.concat(self.config.holidays);
		} else {
			return restricted.map(function(date) {
				return date.format('YYYY-MM-DD');
			});
		}
	},

	/**
	 * returns the valid date range based on the current time
	 * 
	 * @function getDateRange
	 * @return {Object} - object with the minDate and maxDate
	 */
	getDateRange: function() {
		var now = moment();
		var self = DeliverySelection;

		if (self.beforeCutoffTime(now)) {
			var minDate = moment().set('date', now.date() + 1);
		} else {
			var minDate = moment().set('date', now.date() + 2);
		}

		minDate.set('hour', 0).set('minute', 0).set('second', 0).set('millisecond', 0);

		// 13 days outlook
		var maxDate = minDate.clone().add(13, 'days');
		maxDate.set('hour', 23).set('minute', 59).set('second', 59).set('millisecond', 999);

		return { 'minDate': minDate, 'maxDate': maxDate };
	},

	/**
	 * check if the given date is valid
	 * 
	 * @function isDateValid
	 * @param {Object} - moment instance for the date
	 * @param {String} - method name if the date should be validated against a delivery method
	 * @return {boolean}
	 */
	isDateValid: function(date, method) {
		if (!date.isMoment) date = moment(date);

		var self = DeliverySelection;
		var range = self.getDateRange();

		// date is outside of the allowed date range
		if (range.minDate.isAfter(date)) return false;
		if (date.isAfter(range.maxDate)) return false;

		if (method && ( method == 'store' || method == 'wholesale' || method == 'delivery' )) {
			var restricted;

			if (method == 'store') {
				restricted = self.parseRestrictedDates(self.config.delivery_opts.pickup.store, range.minDate, range.maxDate);
			} else if (method == 'wholesale') {
				restricted = self.parseRestrictedDates(self.config.delivery_opts.pickup.wholesale, range.minDate, range.maxDate);
			} else {
				restricted = self.parseRestrictedDates(self.config.delivery_opts.delivery.provider, range.minDate, range.maxDate);

				if (self.zipCode != '') {
					var localRestricted = self.parseRestrictedDates(self.config.delivery_opts.delivery.local, range.minDate, range.maxDate);

					restricted = restricted.filter(function(date) {
						return (localRestricted.indexOf(date) > -1);
					});
				}
			}

			// check if the given date is in the list of retricted dates for the delivery method
			if (restricted && restricted.indexOf(date.format('YYYY-MM-DD')) > -1) return false;
		}

		return true;
	},

	initRefreshAtCutoff: function() {
		//refresh page at cutoff time to remove expired items
		var now = moment();
		var self = DeliverySelection;

		var cutoffTime = moment()
			.tz(self.config.cutoff_time.timezone)
			.set('hour', self.config.cutoff_time.hour)
			.set('minute', self.config.cutoff_time.minute)
			.set('second', 0);

		// we are past the cutoff time for the day, set the refresh for tomorrow
		if (now.isAfter(cutoffTime)) cutoffTime.add(1, 'days');
		
		var timeToCutOff = cutoffTime.unix() - now.unix();

		setTimeout(function() {
			window.location.reload();
		}, timeToCutOff * 1000);
	}
};




/**
 * @class retrieves cart information
 * @namespace DeliverySelection
 */
DeliverySelection.Cart = function() {
	var isCart = (window.location.pathname.indexOf('cart.php') > -1);
	var cartItemSelector = '#cartForm .ProductName';

	/**
	 * check if the current cart have a delivery item
	 *
	 * @function hasDeliveryItem
	 * @return {Promise.<boolean>} - returns try if there is an delivery item in the cart
	 */
	this.hasDeliveryItem = function() {
		var self = this;
		var checkItems = $.Deferred();
		var hasDeliveryItem = false;

		this.getCartItems().then(function(items) {
			// no items in cart
			if (items.length < 1) return checkItems.resolve(hasDeliveryItem);

			// check each item for delivery item attributes
			items.each(function(i, item) {
				if (self.parseHTMLForDeliveryAttrs(item)) {
					hasDeliveryItem = true;
					return false;
				}
			});

			checkItems.resolve(hasDeliveryItem);
		}, function() {
			// failed to get cart content
			checkItems.resolve(hasDeliveryItem);
		});

		return checkItems.promise();
	};

	/**
	 * returns the DOM elements of the items in the cart
	 *
	 * @function getCartItems
	 * @return {Promise.<Object>} - jQuery object of the cart items
	 */
	this.getCartItems = function() {
		var self = this;
		var getCartContent = $.Deferred();

		if (isCart) {
			getCartContent.resolve($(cartItemSelector));
		} else {
			$.get('/cart.php', function(html) {
				if (html) {
					var html = $(html);
					var items = html.find(cartItemSelector);
					getCartContent.resolve(items);
				} else {
					getCartContent.reject();
				}
			});
		}

		return getCartContent.promise();
	};

	this.removeDeliveryItems = function() {
		var self = this;
		removeItems = $.Deferred();
		var removeRequests = [];

		this.getCartItems()
			.then(function(items) {
				// no items in cart
				if (items.length < 1) return removeItems.resolve(true);

				// check each item for delivery item attributes
				items.each(function(i, item) {
					if (self.parseHTMLForDeliveryAttrs(item)) {
						var itemId = $(item).attr('data-item-id');
						if (itemId) {
							removeRequests.push($.get('/cart.php?action=remove&item=' + itemId));
						}
					}
				});

				if (removeRequests.length < 1) return removeItems.resolve(true);

				removeItems.resolve(
					$.when.apply($, removeRequests)
					.then(function() {
						return true;
					}, function() {
						return false;
					})
				);
			});

		return removeItems.promise();
	};

	/**
	 * checks HTML string for pickup / delivery attribute label 
	 *
	 * @function parseHTMLFromDeliveryAttrs
	 * @param {String} html - html content for a cart item
	 * @return {boolean} - returns true if html contains label for pickup / delivery
	 */
	this.parseHTMLForDeliveryAttrs = function(html) {
		html = $(html);

		var attrLabels = $(html).find('.productAttributes').first().find('label');
		var haveDeliveryItems = false;

		if (attrLabels) {
			attrLabels.each(function(index, label) {
				var labelText = $(label).text().replace(/\&\#8203\;/g, '');
				
				if (labelText.indexOf('Picku​p / D​elive​ry') > -1) {
					haveDeliveryItems = true;
				}
			});
		}

		return haveDeliveryItems;
	};
};




/**
 * @class storage adapter for temporarily saving selected options
 * @namespace DeliverySelection
 */
DeliverySelection.Storage = function() {
	var key = '__delivery_opts';
	//var useCookie = (window.location.host.indexOf('mybigcommerce.com') > -1);
	var useCookie = true;

	/**
	 * save data to temporary storage
	 *
	 * @function save
	 * @param {Object} data - data to be saved to storage
	 */
	this.save = function(data) {
		data = JSON.stringify(data);

		if (useCookie) {
			this.setCookie(data);	
		} else {
			window.sessionStorage.setItem(key, data);
		}
	};

	/**
	 * remove data from temporary storage
	 *
	 * @function remove
	 */
	this.remove = function() {
		if (useCookie) {
			this.setCookie('');
		} else {
			window.sessionStorage.removeItem(key);
		}
	};

	/**
	 * retrieve data from temporary storage
	 *
	 * @function get
	 */
	this.get = function() {
		if (useCookie) {
			var data = get_cookie(key);
		} else {
			var data = window.sessionStorage.getItem(key);
		}

		if (data && data != '') {
			return JSON.parse(data);
		} else {
			return false;
		}
	};

	/**
	 * set browser cookie
	 *
	 * @function setCookie
	 * @param {String} data - data to be saved to cookie
	 */
	this.setCookie = function(data) {
		document.cookie = key + '=' + data + ';path=/;domain=.revitalive.com';
	};
};




/**
 * @class controls the multi-step display for the product
 * @namespace DeliverySelection
 * @param {Object} container - display container element
 */
DeliverySelection.Display = function(container) {
	var wrapper = $(container);
	var steps = wrapper.children('div');
	var current = 0;

	var stepsContainer = false;
	
	var nextBtn = false;
	var prevBtn = false;
	var validateFunction = false;
	var inTransition = false;

	/**
	 * show next step
	 *
	 * @function next
	 */
	this.next = function() {
		var currentStep = $(steps[current]);

		// no more steps to display
		if (current >= (steps.length - 1)) return; 
		
		if (typeof validateFunction == 'function') {
			if (!validateFunction(currentStep)) return;
		}

		this.goToStep(current + 1);
	};

	/**
	 * show previous step
	 *
	 * @function prev
	 */
	this.prev = function() {
		var currentStep = $(steps[current]);

		// no more steps to display
		if (current - 1 < 0) return;
		
		this.goToStep(current - 1);
	};

	/**
	 * go to a specific step in the display
	 *
	 * @function goToStep
	 * @param {Number} stepIndex - step index
	 */
	this.goToStep = function(stepIndex) {
		// already transitioning to a different step
		if (inTransition) return false;

		if (stepIndex < 0) stepIndex = 0;
		if (stepIndex > steps.length) stepIndex = steps.length;

		inTransition = true;

		// scroll to the top of the attribute list
		var stepsContainerTopOffset = stepsContainer.position().top;
		$('html, body').animate({'scrollTop': stepsContainerTopOffset - 16 }, 100);

		var nextClass = (stepIndex > current) ? 'left' : 'right';
		
		steps.filter('.active').addClass(nextClass).removeClass('active');
		steps.eq(stepIndex).addClass('active').removeClass('left right completed');

		current = stepIndex;
		this.updateStepsContainer(current);
		this.updateNavButtons(current);

		if (stepIndex >= steps.length - 1) DeliverySelection.stepsComplete = true;

		setTimeout(function() {
			inTransition = false;
		}, 500);
	};

	/**
	 * insert step navigation controls
	 *
	 * @function insertNavigation
	 */
	this.insertNavigation = function() {
		var self = this;
		var navContainer = $('<div />');
		navContainer.addClass('nav-container');

		prevBtn = $('<a href="#" class="btn" disabled="disabled">Previous Step</a>');
		nextBtn = $('<a href="#" class="btn">Next Step</a>');

		navContainer.append(prevBtn).append(nextBtn).insertAfter(container);

		nextBtn.on('click', function(e) {
			self.next();
			return false;
		});

		prevBtn.on('click', function(e) {
			self.prev();
			return false;
		});
	};

	/**
	 * insert step progress display
	 *
	 * @function insertStepsDisplay
	 */
	this.insertStepsDisplay = function() {
		var self = this;

		stepsContainer = $('<div />');
		stepsContainer.addClass('steps-container');

		for (var i = 1; i <= steps.length; i++) {
			if (i == 1) {
				stepsContainer.append($('<span class="active">' + i + '</span>'));
			} else {
				stepsContainer.append($('<span>' + i + '</span>'));
			}
		}

		stepsContainer.insertBefore(container);

		stepsContainer.on('click', 'span.completed', function(e) {
			var stepIndex = parseInt($(this).text()) - 1;
			self.goToStep(stepIndex);	
		});
	};

	/**
	 * update the step progress display
	 *
	 * @function updateStepsContainer
	 * @param {Number} currentStepIndex - index of the current step
	 */
	this.updateStepsContainer = function(currentStepIndex) {
		var stepDisplays = stepsContainer.children();

		stepDisplays.filter(':gt(' + currentStepIndex + ')').removeClass('completed');
		stepDisplays.filter(':lt(' + (currentStepIndex + 1) + ')').addClass('completed');
	};

	/**
	 * update navigation buttons
	 * 
	 * @function updateNavButtons
	 * @param {Number} currentStepIndex - index of the current step
	 */
	this.updateNavButtons = function(currentStepIndex) {
		// previous button
		if (currentStepIndex <= 0) {
			prevBtn.attr('disabled', 'disabled');
		} else {
			prevBtn.removeAttr('disabled');
		}

		// next button
		if (currentStepIndex >= steps.length - 1) {
			nextBtn.attr('disabled', 'disabled');
		} else {
			nextBtn.removeAttr('disabled');
		}

		var navContainer = nextBtn.parents('.nav-container');

		if (currentStepIndex == (steps.length - 1)) {
			navContainer.addClass('done');
		} else {
			navContainer.removeClass('done');
		}
	};

	/**
	 * assign a validation function that will be called before changing steps,
	 * returning false from the function will stop step change
	 * 
	 * @function setValidateFunction
	 * @param {function} func - function to be called on step change
	 */
	this.setValidateFunction = function(func) {
		if (typeof func == 'function') validateFunction = func;
	};

	/**
	 * setup display
	 */
	this.init = function() {
		$(steps[current]).addClass('active');
		var inactive = $(steps).filter(':not(.active)').addClass('right').hide();
		var self = this;

		// insert display for steps
		this.insertStepsDisplay();

		// insert navigation
		this.insertNavigation();

		// show steps
		wrapper.addClass('transition-container');

		setTimeout(function() {
			inactive.show();
		}, 500);
	};
};


/**
 * @class manages form setup for the multi-step display
 * @namespace DeliverySelection.Form
 * @param {Object} rootEle - root element for the form
 */
DeliverySelection.Form = function(rootEle) {
	var wrapper = $(rootEle);
	var options = {};
	var existingOpts = {};
	var datepicker = false;
	var defaultDatepicker = false;

	var dateSelects = {};
	var dateSelectors = {
		'day': 			'.dateselector select.day',
		'month': 		'.dateselector select.month',
		'year': 		'.dateselector select.year'
	};

	var optionsExist = false;

	var DSLib = DeliverySelection;
	
	var displayWrapper = wrapper.find('.productAttributeList').first();
	var display = null;

	/**
	 * replace start date selection with selected start date message
	 *
	 * @function replaceStartDateWithMessage
	 */
	this.replaceStartDateWithMessage = function() {
		var startDateRow = displayWrapper.children('.productAttributeRow.productAttributeConfigurableEntryDate');

		// message is already on screen
		if (startDateRow.children('.selected-message').length > 0) return;

		if (existingOpts.start_date && existingOpts.start_date != '') {
			var startDate = moment(existingOpts.start_date);
			
			this.setDatepickerDate(existingOpts.start_date);

			var message = 'You have selected to start your cleanse on:<span class="highlight">' + startDate.format('MMMM DD, YYYY') + '</span>';
			$('<div class="selected-message">' + message + '</div>').prependTo(startDateRow);
		}

		startDateRow.find('.productAttributeLabel').hide();
	};

	/**
	 * replace delivery options with selected method message 
	 *
	 * @function replaceOptionsWithMessage
	 */
	this.replaceOptionsWithMessage = function() {
		// message is already on screen
		if (options.delivery_option.container.children('.selected-message').length > 0) return;

		if (existingOpts.delivery_option && existingOpts.delivery_option != '') {
			DSLib.selectOptionValue(options.delivery_option.select, existingOpts.delivery_option);
			
			if (existingOpts.pickup_location && existingOpts.pickup_location != '') {
				DSLib.selectOptionValue(options.pickup_location.select, existingOpts.pickup_location);

				if (existingOpts.wholesale_location && existingOpts.wholesale_location != '') {
					// wholesale pickup
					DSLib.selectOptionValue(options.wholesale_location.select, existingOpts.wholesale_location);

					var wholesaleText = options.wholesale_location.select.children('option:selected').first().text().trim();
					var message = 'You have selected to pickup your purchase at the following affiliate location: <span class="highlight">' + wholesaleText + '</span>';
				} else {
					// store pickup
					var message = 'You have selected to pickup your purchase at our store.';
				}
			} else {
				if (existingOpts.delivery_zip && existingOpts.delivery_zip != '') {
					// delivery zip code
					DSLib.selectOptionValue(options.delivery_zip.select, existingOpts.delivery_zip);
				}

				var deliveryText = options.wholesale_location.select.children('option:selected').first().text().trim();
				var message = 'You have selected to have your purchase delivered.';
			}
		}

		if (message) {
			message += '<br />Please proceed to the next step to add this product to your cart.';

			$('<div class="selected-message">' + message + '</div>').prependTo(options.delivery_option.container);				
		}

		DSLib.hideOption(options.delivery_zip);
		DSLib.hideOption(options.delivery_option);
		DSLib.hideOption(options.pickup_location);
		DSLib.hideOption(options.wholesale_location);
	};

	/**
	 * set the element references for the relevant product attributes
	 * 
	 * @function setOptions
	 */
	this.setOptions = function() {
		// get option selectors
		options = {
			'delivery_option': 		DSLib.getOptionFromLabel('Pickup / Delivery'),
			'pickup_location': 		DSLib.getOptionFromLabel('Pickup Location'),
			'wholesale_location': 	DSLib.getOptionFromLabel('Revitalive Affiliate'),
			'delivery_zip': 		DSLib.getOptionFromLabel('ZIP Code'),
			'allergies': 			DSLib.getOptionFromLabel('Allergies'),
			'cleanse_length': 		DSLib.getOptionFromLabel('Length of Cleanse')
		};
	};

	/**
	 * returns the element references for the relevant product attributes
	 * 
	 * @function getOptions
	 */
	this.getOptions = function() {
		return options;
	};

	/**
	 * set the reference to the date selectors
	 * 
	 * @function setDateSelects
	 * @param {Object} selects - object with date selector for day, month and year
	 */
	this.setDateSelects = function(selects) {
		dateSelects = selects;
	};

	/**
	 * replace quantity input with hidden element
	 *
	 * @function replaceQtyInput
	 */
	this.replaceQtyInput = function() {
		// convert quantity input		
		var input = wrapper.find('#qtySelectBox');
		var replacement = $('<input type="hidden" name="qty[]" value="1" />');
		replacement.insertBefore(input);
		input.remove();
	};

	/**
	 * initialize datepicker
	 *
	 * @function initDatePicker
	 */
	this.initDatePicker = function() {
		// rebind date picker
		var startDateRow = displayWrapper.children('.productAttributeConfigurableEntryDate').first();
		defaultDatepicker = startDateRow.find('.picker').first();
		
		// destroy existing picker
		setTimeout(function() {
			defaultDatepicker.datepicker('destroy');
		}, 1000);

		if (!optionsExist) {
			var restriction = DSLib.getDateRange();
			DSLib.minDate = restriction.minDate;
			DSLib.maxDate = restriction.maxDate;

			// init picker
			var picker = $('<div class="startDatePicker" />');
			picker.insertBefore(defaultDatepicker);

			var selects = dateSelects;
			var self = this;

			picker.datepicker({
				'dateFormat': 'yy-mm-dd',
				'defaultDate': DSLib.minDate.toDate(),
				'minDate': DSLib.minDate.toDate(),
				'maxDate': DSLib.maxDate.toDate(),
				'beforeShowDay': DSLib.filterRestrictedDate,
				'onSelect': function(dateText, picker) {
					self.setDatepickerDate(dateText);
				}
			});

			datepicker = picker;

			// set the default date in the date select dropdowns
			this.setDatepickerDate(DSLib.minDate.format('YYYY-MM-DD'));
		}
	};

	/**
	 * set a date to the hidden date selects and default validation inputs
	 * 
	 * @function setDatepickerDate
	 * @param {String} dateText - date formatted in YYYY-MM-DD
	 */
	this.setDatepickerDate = function(dateText) {
		var selected = dateText.split('-');
		var year = selected[0];
		var month = selected[1];
		var day = selected[2];

		dateSelects.day.children('option:selected').removeAttr('selected');
		dateSelects.day.children('option[value="' + day + '"]').attr('selected', 'selected');

		dateSelects.month.children('option:selected').removeAttr('selected');
		dateSelects.month.children('option[value="' + month + '"]').attr('selected', 'selected');

		dateSelects.year.children('option:selected').removeAttr('selected');
		dateSelects.year.children('option[value="' + year + '"]').attr('selected', 'selected');

		// update the hidden fields for validation
		defaultDatepicker.val(dateText);
		defaultDatepicker.siblings('.validation').val(dateText);
	};

	/**
	 * move the add button to the last step of the display
	 *
	 * @function relocateAddButton
	 */
	this.relocateAddButton = function() {
		// merge add to cart with start date step
		var addBtn = $('.AddCartButton');
		var addBtnRow = addBtn.parents('.DetailRow').first();

		displayWrapper.children('.productAttributeRow.productAttributeConfigurableEntryDate').append(addBtn);
		addBtnRow.remove();
	};

	/**
	 * combine all the relevant delivery options into a single step in the display
	 *
	 * @function combineDeliveryOptions
	 */
	this.combineDeliveryOptions = function() {
		// merge allergies into the cleanse length step
		options.allergies.container.children().appendTo(options.cleanse_length.container);
		options.allergies.container.remove();

		// merge delivery options into a single step
		options.pickup_location.container.children().appendTo(options.delivery_option.container);
		
		// insert link to view locations
		$('<span class="location-link"><a href="/pickup-locations/" target="_blank">View Locations</a></span>').insertAfter(options.wholesale_location.select);
		options.wholesale_location.container.children().appendTo(options.delivery_option.container);
		options.delivery_zip.container.children().appendTo(options.delivery_option.container);

		// remove old containers
		options.pickup_location.container.remove();
		options.wholesale_location.container.remove();
		options.delivery_zip.container.remove();

		// hide dependent option rows
		DSLib.hideOption(options.pickup_location);
		DSLib.hideOption(options.wholesale_location);
		DSLib.hideOption(options.delivery_zip);

		if (!optionsExist) {
			// add event listener to show proper option fields
			options.delivery_option.select.on('change', { 'options': options, 'datepicker': datepicker }, DSLib.handleDeliveryOptionChange);
			options.pickup_location.select.on('change', { 'options': options, 'datepicker': datepicker }, DSLib.handlePickupLocationChange);
		}
	};

	/**
	 * checks if there are options saved in temporary storage 
	 *
	 * @function detectExistingOptions
	 */
	this.detectExistingOptions = function() {
		var storage = new DSLib.Storage();
		var opts = storage.get();

		if (opts !== false) {
			optionsExist = true;
			existingOpts = opts;
		}
	};

	/**
	 * validate the data selection on the current step
	 * 
	 * @function validateStep
	 * @param {Object} step - the step to validate
	 */
	this.validateStep = function(step) {
		var currentStep = false;

		$.each(options, function(key, option) {
			if ($(option.container).is(step)) currentStep = key;
		});

		if (currentStep == 'cleanse_length') {
			return validateCleanseLength();
		} else if (currentStep == 'delivery_option') {
			return validateDeliveryOption();
		} else {
			return validateStartDate();
		}

		return true;
	};

	/**
	 * validates every step in the display
	 * 
	 * @function validate
	 */
	this.validate = function() {
		var self = this;
		var isValid = true;

		displayWrapper.children('.productAttributeRow').each(function(i, step) {
			if (!self.validateStep(step)) {
				isValid = false;
				return false;
			}
		});

		return isValid;
	};

	/**
	 * validates the cleanse length selection
	 * 
	 * @function validateCleanseLength
	 * @access private
	 */
	var validateCleanseLength = function() {
		if ($(options.cleanse_length.select).val() == '') {
			alert('Please select your length of cleanse.');
			$(options.cleanse_length.select).focus();
			return false;
		}

		return true;
	};

	/**
	 * validates the delivery options
	 * 
	 * @function validateDeliveryOption
	 * @access private
	 */
	var validateDeliveryOption = function() {
		var val = $(options.delivery_option.select).val();
		DSLib.zipCode = '';

		if (val == '') {
			alert('Please select an option for Pickup / Delivery.');
			$(options.delivery_option.select).focus();
			return false;
		}

		var selected = $(options.delivery_option.select).children('[selected]').text();

		if (selected.toLowerCase().indexOf('pickup') > -1) {
			return validatePickupLocation();
		} else {
			return validateDeliveryZipCode();
		}
	};

	/**
	 * validates the delivery zip code
	 * 
	 * @function validateDeliveryZipCode
	 * @access private
	 */
	var validateDeliveryZipCode = function() {
		var zip = $(options.delivery_zip.select).val();

		if (zip == '') {
			alert('Please enter your zip code to continue.');
			$(options.delivery_zip.select).focus();
			return false;
		}

		if (datepicker) {
			// update restricted dates
			var restrictedDates = DSLib.parseRestrictedDates(DSLib.config.delivery_opts.delivery.provider);

			var allowedZipCodes = DSLib.config.delivery_opts.delivery.local.allowed_zip_code;
			if (restrictedDates.length > 0 && allowedZipCodes.indexOf(zip) > -1) {
				var restrictedLocalDates = DSLib.parseRestrictedDates(DSLib.config.delivery_opts.delivery.local);
			
				restrictedDates = restrictedDates.filter(function(date) {
					return (restrictedLocalDates.indexOf(date) > -1);
				});
			}
			
			DSLib.zipCode = zip;
			DSLib.restrictedDates = restrictedDates;

			datepicker.datepicker('refresh');
		}

		return true;
	};

	/**
	 * validates the pickup location
	 * 
	 * @function validatePickupLocation
	 * @access private
	 */
	var validatePickupLocation = function() {
		var val = $(options.pickup_location.select).val();

		if (val == '') {
			alert('Please select an option for Pickup Location.');
			$(options.pickup_location.select).focus();
			return false;
		}

		var selected = $(options.pickup_location.select).children('[selected]').text();

		if (selected.toLowerCase().indexOf('affiliate') > -1) {
			return validateWholesaleLocation();
		}

		return true;
	};

	/**
	 * validates the wholesale location
	 * 
	 * @function validateWholesaleLocation
	 * @access private
	 */
	var validateWholesaleLocation = function() {
		var val = $(options.wholesale_location.select).val();

		if (val == '') {
			alert('Please select an option for Wholesale Location.');
			$(options.wholesale_location.select).focus();
			return false;
		}

		return true;
	};

	/**
	 * validates the start date selection
	 * 
	 * @function validateStartDate
	 * @access private
	 */
	var validateStartDate = function() {
		$.each(dateSelects, function(key, select) {
			var val = $(select).val();

			if (val == '') {
				alert('Please select your start date.');
				return false;
			}
		});

		return true;
	};

	/**
	 * setup form
	 *
	 * @function init
	 */
	this.init = function() {
		this.detectExistingOptions();
		this.setOptions();
		this.replaceQtyInput();
		this.relocateAddButton();
		this.initDatePicker();
		this.combineDeliveryOptions();
		
		if (optionsExist) {
			this.replaceOptionsWithMessage();
			this.replaceStartDateWithMessage();
		}

		// setup display container
		this.display = new DSLib.Display(displayWrapper);
		this.display.setValidateFunction(this.validateStep);
		this.display.init();
	};
};




/**
 * Delivery Option Selection
 * @requires jQuery
 * @requires moment
 * @requires moment-timezone
 */
(function($, window) {
	var selects = {};
	var options = {};
	var form = null;

	var elemSelectors = {
		'day': 			'.dateselector select.day',
		'month': 		'.dateselector select.month',
		'year': 		'.dateselector select.year'
	};

	/**
	 * replacement for the check_add_to_cart function, adds validation for delivery related options 
	 *
	 * @function checkAddToCart
	 * @param {Object} form
	 * @param {boolean} required
	 * @return {boolean} - true if options are valid
	 */
	function checkAddToCart(formElem, required) {
		if (form) {
			// validate delivery options
			if (!form.validate()) {
				return false;
			}
		}

		if (!DeliverySelection.stepsComplete) {
			form.display.next();
			return false;
		}

		var method = 'delivery';

		if (options.delivery_option.select.val() != '' && options.delivery_option.select.find('option:checked').text().toLowerCase() != 'delivery') {
			if (options.pickup_location.select.val() != '') {
				if (options.wholesale_location.select.val() != '') method = 'wholesale';
				else method = 'store';
			}
		}

		// saved date is no longer valid
		if (!DeliverySelection.isDateValid($('.picker').val(), method)) {
			return false;
		}

		// check configurable fields
		if (!CheckProductConfigurableFields(formElem)) return false;

		// validate option fields
		var attributesValidated = $(formElem).validate().form();
		if (!attributesValidated) return false;
		
		// check qty
		if (!CheckQuantityLimits(formElem)) return false;

		if(required && !$(formElem).find('.CartVariationId').val()) {
			alert(lang.OptionMessage);
			var select = $(formElem).find('select').get(0);
			if(select) {
				select.focus();
			}
			var radio = $(formElem).find('input[type=radio]').get(0);
			if(radio) {
				radio.focus();
			}
			return false;
		}

		if (!CheckEventDate()) return false;
		
		// save delivery option
		var delivery_opts = {
			'delivery_option': options.delivery_option.select.val(),
			'pickup_location': '',
			'wholesale_location': '',
			'delivery_zip': '',
			'start_date': $('.picker').val()
		};

		if (method != 'delivery') {
			delivery_opts.pickup_location = options.pickup_location.select.val();
			if (method == 'wholesale') delivery_opts.wholesale_location = options.wholesale_location.select.val();
		} else {
			delivery_opts.delivery_zip = options.delivery_zip.select.val();
		}

		// remove unused data from submitted data so it doesn't show up in cart
		options.delivery_zip.select.val('');

		var storage = new DeliverySelection.Storage();
		
		// check form data with default validation
		if (!config.FastCart) {
			storage.save(delivery_opts);
			return true;
		}

		$('#productDetailsAddToCartForm').ajaxSubmit({
			'data': {
				'fastcart': 1,
				'ajaxsubmit': 1
			},
			'type': 'post',
			'iframe': true,
			'dataType': 'json',
			'success': function(data) {
				if (data.success) {
					// save selected options on success
					storage.save(delivery_opts);

					// remove unused data from submitted data so it doesn't show up in cart
					options.delivery_zip.select.val(delivery_opts.delivery_zip);

					form.detectExistingOptions();
					form.replaceOptionsWithMessage();
					form.replaceStartDateWithMessage();

					$('.startDatePicker').datepicker('destroy');

					// show popup
					_showFastCart({ 'data': data });

					//refresh page at cutoff time to remove expired items
					DeliverySelection.initRefreshAtCutoff();
				} else if (data.redirect) {
					window.location.href = data.redirect;
				}
			}
		});
		
		return false;
	}

	/**
	 * custom handler for checkout step submission response
	 *
	 * @function FilteredCheckoutResponseHandler
	 * @param {Object} response - JSON response from BigCommerce for Express Checkout step submission
	 */
	function FilteredCheckoutResponseHandler(response) {
		var pickupMethodLabel = 'In-store Pickup';
		var localDeliveryMethodLabel = 'Local Driver';

		if (response.stepContent != undefined) {
			$.each(response.stepContent, function(i, step) {
				if (step.id && step.id == 'ShippingProvider') {
					var stepContent = $(step.content);
					var storage = new DeliverySelection.Storage();
					var selectedOptions = storage.get();

					var selectedMethod = false;
					var removeMethods = [];

					if (selectedOptions.pickup_location) {
						selectedMethod = pickupMethodLabel;
					} else {
						removeMethods.push(pickupMethodLabel);

						// delivery method
						if (selectedOptions.start_date) {
							// check configuration for local driver
							if (
								document.__bapo_config__.delivery_opts && 
								document.__bapo_config__.delivery_opts.delivery && 
								document.__bapo_config__.delivery_opts.delivery.local
							) {
								var startDate = moment( selectedOptions.start_date );
								var localOpts = document.__bapo_config__.delivery_opts.delivery.local;
								var dateValue = false;

								// remove local driver if it is not available on the start date
								if (localOpts.restriction_type == 'dayOfWeek') dateValue = startDate.format('ddd');
								else if (localOpts.restriction_type == 'dayOfMonth') dateValue = startDate.date();
								else if (localOpts.restriction_type == 'date') dateValue = startDate.format('YYYY-MM-DD');

								if (dateValue && localOpts.restricted_values.indexOf(dateValue) > -1) removeMethods.push(localDeliveryMethodLabel);
							} else {
								// local driver option not configured
								removeMethods.push(localDeliveryMethodLabel);
							}
						}
					}

					if (stepContent && (selectedMethod || removeMethods.length > 0)) {
						var shippingMethods = stepContent.find('.ShippingProviderList li');
						if (shippingMethods) {
							$(shippingMethods).each(function(i, method) {
								method = $(method);
								var methodText = method.find('.ShipperName').first().text();

								if (selectedMethod) {
									if (methodText.indexOf(selectedMethod) < 0) {
										method.remove();
									} else {
										method.find('input[type=radio]').first().attr('checked', 'checked');
									}
								} else {
									if (removeMethods.indexOf(methodText) > -1) method.remove();
								}
							});
						}

						step.content = stepContent.get(0).outerHTML;
					}
				}
			});
		}

		ExpressCheckout._HandleResponse(response);
	}

	function initProductDetails() {
		// setup form
		$.each(elemSelectors, function(name, selector) {
			selects[name] = $(selector).first();
		});

		// no date selectors on the current page
		if (selects.day.length < 1 || selects.month.length < 1 || selects.year.length < 1) return;

		// replace form validation function
		DeliverySelection.replaceAddToCartFunction(checkAddToCart);

		form = new DeliverySelection.Form($('#ProductDetails'));
		form.setDateSelects(selects);
		form.init();

		options = form.getOptions();
	}

	$(function() {
		// no delivery options defined
		if (!document.__bapo_config__) return;

		var DS = DeliverySelection;
		var path = window.location.pathname;

		var storage = new DS.Storage();
		var opts = storage.get();

		// set configuration options
		DS.config = document.__bapo_config__;

		if (path.indexOf('finishorder.php') > -1) {
			// remove saved selection from storage on finish order page
			return storage.remove();
		} else if (path.indexOf('checkout.php') > -1) {
			// replace response handler for checkout
			DS.replaceCheckoutResponseHandler(FilteredCheckoutResponseHandler);

			// check if selected date is still valid
			var cart = new DS.Cart();

			if (opts) {
				var method = 'delivery';

				if (opts.pickup_location) {
					if (opts.wholesale_location) method = 'wholesale';
					else method = 'store';
				}

				// saved date is no longer valid
				if (!DS.isDateValid(opts.start_date, method)) {
					storage.remove();
					
					return cart.removeDeliveryItems()
						.then(function() {
							return cart.getCartItems();
						})
						.then(function(items) {
							if (items.length > 0) window.location.reload();
							else window.location.assign('/cart.php');
						});
				}
			}
		} else if (path.indexOf('cart.php') > -1) {
			// check if there are still cleanses in the cart, otherwise clear selection
			var cart = new DS.Cart();

			cart.hasDeliveryItem()
				.then(function(hasDeliveryItem) {
					if ( !hasDeliveryItem ) {
						// clear saved selection if there are no items in the cart
						storage.remove();
					} else {
						if (opts) {
							// check if selected date is still valid
							var method = 'delivery';

							if (opts.pickup_location) {
								if (opts.wholesale_location) method = 'wholesale';
								else method = 'store';
							}

							// saved date is no longer valid
							if (!DS.isDateValid(opts.start_date, method)) {
								storage.remove();
								
								return cart.removeDeliveryItems().then(function() {
									window.location.reload();
								});
							} else {
								//refresh page at cutoff time to remove expired items
								DS.initRefreshAtCutoff();
							}
						}
					}
				});

			return;
		} else if ($('#ProductDetails').length > 0) {
			// product details page
			var cart = new DS.Cart();
			
			if (opts) {
				var method = 'delivery';

				if (opts.pickup_location) {
					if (opts.wholesale_location) method = 'wholesale';
					else method = 'store';
				}

				// saved date is no longer valid
				if (!DS.isDateValid(opts.start_date, method)) {
					storage.remove();
					
					return cart.removeDeliveryItems()
						.then(initProductDetails);
				}
			}

			cart.hasDeliveryItem()
				.then(function(hasDeliveryItem) {
					// clear saved selection if there are no items in the cart
					if ( !hasDeliveryItem ) storage.remove();
				})
				.then(initProductDetails);
		}
	});
})(window.jQuery, window);