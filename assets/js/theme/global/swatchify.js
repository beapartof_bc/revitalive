import $ from 'jquery';
import utils from '@bigcommerce/stencil-utils';
// add the line below to disable check
/* eslint-disable */
    setTimeout(initSwatchify,100);
    var itemsAdded = 0;
    //init Swatchify
    //::: Looks at the product container for a count of product items using the class .product
    //::: for each product gets pushed into a promise
    function initSwatchify(){
        var items = [];
        var pagelisting = document.getElementById("product-listing-container");
        
        //if on a grid list of products run for each product in a promise
        if(document.body.contains(pagelisting)){          
            var products = document.querySelectorAll('.product');
            for(var i=0; i<products.length; i++){
                var prodItem = products[i];
                
                var id = $(prodItem).attr('data-id');
                
                var prodItem = document.querySelector('.product[data-id="'+id+'"]');
    
                //creates swatch box per each product 
                var swatchBoxInfo = document.createElement("div");
                swatchBoxInfo.style.cssText += "display:block;clear:both;width:100%;position:relative;min-height: 50px;margin-top: -30px;";
                swatchBoxInfo.setAttribute('id','swatchBoxInfo-'+id);
                swatchBoxInfo.className += "swatchBoxInfo";
                prodItem.appendChild(swatchBoxInfo);
                var dataSet = {
                    'product_id': id
                };               
                
                var promise = dataSet;
                items.push(promise);
                
            }
            
            itemsAdded = items.length;
            swatchifyItem(items);
        }   
    } 
    
    //swatchify promise
    //::: cycles through the promises of product items
    //::: MIGHT NOT NEED
    function swatchifyItem(items, prev){
        if (!prev) {
            var deferred = new $.Deferred();
            deferred.resolve();
            return swatchifyItem(items, deferred.promise());
        }
    
        return prev
            .then(function() {
                var _wait = new $.Deferred();
                                
                setTimeout(function() {
                    _wait.resolve();
                }, 10);
                
                return _wait.promise();
            })
            .then(function() {
                
                var data = items.shift();
                if(items!=itemsAdded){
                    //console.log('submitting item to cart');
                    swatchifyItem(items, getSwatchInfo(data));
                }else{
                    console.log('done swatches');
                }
            });
    }      
                
    //sends product to promise and builds swatches     
    //::: builds the swatch div for each product item
    function getSwatchInfo(data){            
        itemsAdded--;
        var i = data.product_id;
        
            //returns product data to grab swatches and binds the switch
            var dataDump = (function(d, key){
                var dataSet = key;
                
                var elements = $(dataSet);
                var found = $('.form-field[data-product-attribute="swatch"]', elements);
                var foundSize = $('.form-field[data-product-attribute="set-select"]', elements);
                
                var colors = found;
                if(colors != null){                 
                    var swaption = colors;
                    
                    $('#swatchBoxInfo-'+i).append(colors);
                    $('#swatchBoxInfo-'+i).append(foundSize);
                    $('#swatchBoxInfo-'+i).children('.form-field[data-product-attribute="set-select"]').css('display','none');
                    $('#swatchBoxInfo-'+i).find('.form-option').attr('data-id',i);
                    
                    var setName = $('#swatchBoxInfo-'+i).children('.form-field[data-product-attribute="set-select"]').children('select').attr('name');
                    $('#swatchBoxInfo-'+i).find('.form-option').attr('data-set',setName);
                    var setId = $('#swatchBoxInfo-'+i).children('.form-field[data-product-attribute="set-select"]').children('select').children('option:nth-child(2)').val();
                    $('#swatchBoxInfo-'+i).find('.form-option').attr('data-set-id',setId);
                    
                    $('#swatchBoxInfo-'+i).find('.form-option').bind('click', swapper);
                }else{
                    $('#swatchBoxInfo-'+i).html('');
                }
            });
            
            utils.api.product.getById(data.product_id, data, dataDump);
    }
    
    
    
    //Color swap
    //::: This should just be for use after all swatches are loaded
    //::: GOOD
    function swapper(){
        var prodId = $(this).attr('data-id');
        var att = $(this).prev('.form-radio').attr('name');
        var attrValue = $(this).attr('data-product-attribute-value');
        
        var setName = $(this).attr('data-set');
        var setId = $(this).attr('data-set-id');
                
        var data = {
            action: 'add',
            product_id: prodId,
            'qty[]': 1
        };
        
        data[att] = attrValue;
        data[setName] = setId;
        
        $.ajax({
            url: '/remote/v1/product-attributes/'+prodId+'',
            type: 'POST',
            dataType: "json",
            data: data,
            success: function(color){               
                var image = color.data.image.data;
                var res = image.replace("{:size}", "500x659");
                
                $('.product[data-id="'+prodId+'"]').find('.card-image').attr('src',res);
            }
        });
    }
// add the line below to re-enable check
/* eslint-enable */
